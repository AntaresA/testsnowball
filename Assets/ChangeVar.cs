﻿using UnityEngine;
using UnityEngine.UI;

public class ChangeVar : MonoBehaviour
{
    public Text HippoSpeed;
    public Text EnemySpeed;
    public Text HippoCD;
    public Text EnemyCD;
    public Text EnemyHeartToWin;
    public Text Lvl1Reward;
    public Text Lvl2Reward;
    public Text Lvl3Reward;

    
    private void OnEnable()
    {
        GlobalVar.HippoSpeed = int.Parse(HippoSpeed.text);
        GlobalVar.EnemySpeed = int.Parse(EnemySpeed.text);
        GlobalVar.HippoCD = float.Parse(HippoCD.text);
        GlobalVar.EnemyCD = float.Parse(EnemyCD.text);
        GlobalVar.EnemyHeartToWin = int.Parse(EnemyHeartToWin.text);
        GlobalVar.Lvl1Reward = int.Parse(Lvl1Reward.text);
        GlobalVar.Lvl2Reward = int.Parse(Lvl2Reward.text);
        GlobalVar.Lvl3Reward = int.Parse(Lvl3Reward.text);
    }
}