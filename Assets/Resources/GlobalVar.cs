﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GlobalVar : MonoBehaviour
{
    public static int HippoSpeed;
    public static int EnemySpeed;
    public static int HippoHP;
    public static int enemyHP;
    public static float HippoCD;
    public static float EnemyCD;
    public static int EnemyHeartToWin;
    public static int ChanceToStop;
    public static int Lvl1Reward = 1;
    public static int Lvl2Reward = 2;
    public static int Lvl3Reward = 3;
}
