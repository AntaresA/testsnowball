﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PowerSB : MonoBehaviour
{
    public Image LoadingImg;
    public float Power;
    void Start()
    {
        Power = 0;
        StartCoroutine(PowerChange());
    }

    IEnumerator PowerChange()
    {
        while (true) 
        {
            while (Power < 1f)
            {
                Power += 0.01f;
                LoadingImg.fillAmount = Power;
                yield return typeof(WaitForEndOfFrame);
            }
            
            while (Power >= 0.1f)
            {
                Power -= 0.01f;
                LoadingImg.fillAmount = Power;
                yield return typeof(WaitForEndOfFrame);
            }

            yield return null;
        }
        
    }
}
