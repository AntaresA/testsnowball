﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySB : MonoBehaviour
{
    [SerializeField]
    private Rigidbody2D _rigidbody;
    [SerializeField]
    public float speed;

    [SerializeField] 
    public GameController GM;
    
    
    private bool ready;

    public void ForceSB(Vector3 startpoint, Vector3 point)
    {
        StopAllCoroutines();
        _rigidbody.position = new Vector2(0, 25);
        StartCoroutine(MooveToward(startpoint, point));
    }
    
    void OnTriggerEnter2D(Collider2D col)
    {
        if (col.CompareTag("SnowballHippo"))
        {
            StopAllCoroutines();
            ready = true;
            _rigidbody.position = new Vector2(0, 25);
            gameObject.SetActive(false);
        }

        if (col.CompareTag("Player"))
        {
            StopAllCoroutines();
            ready = true;
            _rigidbody.position = new Vector2(0, 25);
            GM.HippoHp();
            gameObject.SetActive(false);
        }
    }
    
    IEnumerator MooveToward(Vector3 startpoint, Vector3 point)
    {
        ready = false;
        _rigidbody.position = startpoint;
        transform.position = _rigidbody.position;
        while(transform.position != point)
        {
            var position = _rigidbody.position;
            _rigidbody.position = Vector2.MoveTowards(position, point ,Time.deltaTime * speed);
            
            yield return null;
        }
        
        _rigidbody.position = new Vector2(0, 25);
        ready = true;
        gameObject.SetActive(false);
        yield return null;
    }
    
}
