﻿using System.Collections;
using System.Collections.Generic;
using Game;
using UnityEngine;
using UnityEngine.UI;
using Random = System.Random;

public class GameController : MonoBehaviour
{
    [SerializeField] 
	private Hippo Hippo;
	[SerializeField] 
	private HippoSB SnowballHippo;
	[SerializeField] 
	private SnowBallCD SnowCD;
	[SerializeField] 
	private PowerSB SnowballPower;
	
	[SerializeField] 
	private Text HpHippo;
    [SerializeField] 
	private Text HpEnemy;
	

	[SerializeField] private int HippoSpeed;
	[SerializeField] private int EnemySpeed;
	[SerializeField] private int HippoHP;
	[SerializeField] private int enemyHP;
	[SerializeField] private float HippoCD;
	[SerializeField] private float EnemyCD;
	[SerializeField] private int EnemyHeartToWin;
	[SerializeField] private int ChanceToStop;
	
	[SerializeField]
	private string buttonNameA = "Attack";

	[Header("List of Enemy (top, middle, bot)")]
	[SerializeField] private GameObject enemy;

	[SerializeField] private GameObject _enemySbGameObject;
	[SerializeField] private EnemySB _enemySb;
	
	[SerializeField] private float enemy1Pos;
	[SerializeField] private float enemy2Pos;
	[SerializeField] private float enemy3Pos;
	
	[SerializeField] private GameObject enemyGameObject1;
	[SerializeField] private GameObject enemyGameObject2;
	[SerializeField] private GameObject enemyGameObject3;

	[SerializeField] private PauseWindow _pauseWindow;
	[SerializeField] private PauseWindow _looseWindow;
	[SerializeField] private WinWindow _winWindow;

	//[SerializeField] private PauseWindow _pauseWindow;
		
	private Enemy enemy1;
	private Enemy enemy2;
	private Enemy enemy3;

	private List<Enemy> Enemys;


    void Awake()
    {
	    HippoSpeed = GlobalVar.HippoSpeed > 0 ? GlobalVar.HippoSpeed : HippoSpeed;
	    EnemySpeed = GlobalVar.EnemySpeed > 0 ? GlobalVar.EnemySpeed : EnemySpeed;
		HippoHP = GlobalVar.HippoHP > 0 ? GlobalVar.HippoHP : HippoHP;
		enemyHP = GlobalVar.enemyHP > 0 ? GlobalVar.enemyHP : enemyHP;
		HippoCD = GlobalVar.HippoCD > 1 ? GlobalVar.HippoCD : HippoCD;
		EnemyCD = GlobalVar.EnemyCD > 1 ? GlobalVar.EnemyCD : EnemyCD;
		EnemyHeartToWin = GlobalVar.EnemyHeartToWin > 0 ? GlobalVar.EnemyHeartToWin : EnemyHeartToWin;
		ChanceToStop = GlobalVar.ChanceToStop > 0 ? GlobalVar.ChanceToStop : ChanceToStop;
	    
	    
	    Hippo.Speed = HippoSpeed;
	    Hippo.Hp = HippoHP;
	    SnowCD.snowballGlobalCD = HippoCD;

	    enemyHP = EnemyHeartToWin;
	    HpEnemy.text = EnemyHeartToWin + "/" + enemyHP;
	    HpHippo.text = "" + HippoHP;
	    
    }

    void Start()
    {
	   StartCoroutine(EnemySpawn());
	   _enemySbGameObject = Instantiate(_enemySbGameObject, new Vector3(0,enemy1Pos,0), Quaternion.identity);
	   _enemySb = _enemySbGameObject.GetComponent<EnemySB>();
	   _enemySb.speed = HippoSpeed * 2;
	   _enemySb.GM = this.gameObject.GetComponent<GameController>();
	   _enemySbGameObject.SetActive(false);
	   StartCoroutine(EnemySnowballController());
    }
    void Update()
    {
	    if (SimpleInput.GetButtonDown(buttonNameA))
	    {
		    if (SnowballHippo.state == HippoSBstate.Wait && SnowCD.snowballOnCd == false) 
		    {
			    SnowballHippo.gameObject.SetActive(true);
			    SnowballHippo.power = SnowballPower.Power * 8;
			    SnowballHippo.ForceSB();
		    }
		    else
		    {
			    //TODO: Добавить подсказку "Снежок еще не готов"
		    }
	    }
    }

    public void EnemyHp(int reward)
    {
	    if (enemyHP > reward)
	    {
		    enemyHP -= reward;
		    HpEnemy.text = "" + enemyHP;
	    }
	    else
	    {
		    Debug.Log("WIN");
		    _winWindow.starQuantity = HippoHP;
		    _winWindow.gameObject.SetActive(true);
		    
	    }
    }

    public void HippoHp()
    {
	    if (HippoHP > 1)
	    {
		    HippoHP -= 1;
		    HpHippo.text = "" + HippoHP;
	    }
	    else
	    {
		    Debug.Log("Loose");
		    _looseWindow.gameObject.SetActive(true);
		    
	    }
    }


    IEnumerator EnemySpawn()
    {
	    enemyGameObject1 = Instantiate(enemy, new Vector3(0,enemy1Pos,0), Quaternion.identity);
	    yield return new WaitForSeconds(0.1f);
	    enemyGameObject2 = Instantiate(enemy, new Vector3(0,enemy2Pos,0), Quaternion.identity);
	    yield return new WaitForSeconds(0.1f);
	    enemyGameObject3 = Instantiate(enemy, new Vector3(0,enemy3Pos,0), Quaternion.identity);
	    
	    enemy1 = enemyGameObject1.GetComponent<Enemy>();
	    enemy2 = enemyGameObject2.GetComponent<Enemy>();
	    enemy3 = enemyGameObject3.GetComponent<Enemy>();

	    enemy1.GM = this.gameObject.GetComponent<GameController>();
	    enemy2.GM = this.gameObject.GetComponent<GameController>();
	    enemy3.GM = this.gameObject.GetComponent<GameController>();

	    enemy1.type = EnemyType.lvl1;
	    enemy2.type = EnemyType.lvl1;
	    enemy3.type = EnemyType.lvl1;
	    
	    Debug.Log(enemy1.speed);
	    enemy1.speed = EnemySpeed;
	    Debug.Log(enemy1.speed);
	    enemy2.speed = EnemySpeed;
	    enemy3.speed = EnemySpeed;

	    enemy1.chanceToStop = ChanceToStop;
	    enemy2.chanceToStop = ChanceToStop;
	    enemy3.chanceToStop = ChanceToStop;
	    
	    Enemys = new List<Enemy>(3){enemy1, enemy2, enemy3};
	    yield return null;
    }

    IEnumerator EnemySnowballController()
    {
	    yield return new WaitForSeconds(EnemyCD);
	    var rnd = new Random();
		while (true)
		{
			int activeEnemy = rnd.Next(0, 3);
			Debug.Log(activeEnemy + " !!! " + Enemys[activeEnemy]);
			_enemySb.gameObject.SetActive(true);
			_enemySb.ForceSB(Enemys[activeEnemy].transform.position, Hippo.transform.position);
			yield return new WaitForSeconds(EnemyCD);
		    yield return null;
	    }
		yield return null;
    }
    
    void OnGUI()
    {
	    if (Input.GetKeyUp(KeyCode.Escape))
	    {
		    _pauseWindow.gameObject.SetActive(true);
	    }
    }
}
