﻿using Spine.Unity;
using UnityEngine;

namespace Game
{
    public class Hippo : MonoBehaviour
    {
        [SerializeField]
        private string name;
        [SerializeField]
        private int hp;
        [SerializeField]
        private int speed;
        [SerializeField]
        private Vector3 hippoStartPosition;
        [SerializeField]
        private SkeletonAnimation state;

        public string horizontalAxis = "Horizontal";
    
        private float inputHorizontal;
        private Rigidbody2D m_rigidbody;

    
        void Update()
        {
            inputHorizontal = SimpleInput.GetAxis( horizontalAxis );
        }

        void FixedUpdate()
        {
            MooveController();
        }
    
        private void Awake()
        {
            m_rigidbody = GetComponent<Rigidbody2D>();
            //state.AnimationName = "Idle";
            name = "Hippo";
            HippoPosition = new Vector3(0, -12, 0);
            transform.position = hippoStartPosition;
            Debug.Log($"I`m Awake of {name}, HP: {hp}, speed: {speed}");
        }
    
        void MooveController()
        {
            if (transform.position.x <= -9 || transform.position.x >= 9)
            {
                var mooveVelocity = (new Vector2(transform.position.x * (-0.2f), 0) * speed);
                m_rigidbody.MovePosition(m_rigidbody.position + mooveVelocity * Time.deltaTime);
            }
            else if (inputHorizontal != 0)
            {
                state.AnimationName = "run";
                if (inputHorizontal > 0f)
                {
                    transform.rotation = Quaternion.Euler(0,0,0);
                }
                else
                {
                    transform.rotation = Quaternion.Euler(0,180,0);
                }
                
                var mooveVelocity = (new Vector2(inputHorizontal, 0) * speed);
                m_rigidbody.MovePosition(m_rigidbody.position + mooveVelocity * Time.deltaTime);
            }
            else
            {
                state.AnimationName = "Idle";
            }
        }
        public int Hp
        {
            get => hp;
            set => hp = value;
        }
        public int Speed
        {
            get => speed;
            set => speed = value;
        }
        public Vector3 HippoPosition
        {
            get { return new Vector2(m_rigidbody.position.x, m_rigidbody.position.y); }
            set => hippoStartPosition = value;
        }
    }
}