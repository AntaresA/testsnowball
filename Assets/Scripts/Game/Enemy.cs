﻿using System.Collections;
using Spine.Unity;
using UnityEngine;
using Random = System.Random;

namespace Game
{
    public class Enemy : MonoBehaviour
    {
        public GameController GM;
        public EnemyType type;
        public SkeletonAnimation state;
        public float speed;
        public int chanceToStop;
        public float startPos;

        private int reward;

        private Rigidbody2D _rigidbody;

        private void Awake()
        {
            var xPos = transform.position.x;
            xPos = startPos;
            _rigidbody = GetComponent<Rigidbody2D>();
        }

    
        
        void TypeSwitch()
        {
            if (type == EnemyType.lvl1)
            {
                state.Skeleton.SetSkin("CatPapa"); 
                state.Skeleton.SetSlotsToSetupPose();
                state.AnimationState.Apply(state.skeleton);
                reward = GlobalVar.Lvl1Reward;
            }else if (type == EnemyType.lvl2)
            {
                state.Skeleton.SetSkin("DogPapa"); 
                state.Skeleton.SetSlotsToSetupPose();
                state.AnimationState.Apply(state.skeleton);
                reward = GlobalVar.Lvl2Reward;
            }else
            {
                state.Skeleton.SetSkin("LeoPapa"); 
                state.Skeleton.SetSlotsToSetupPose();
                state.AnimationState.Apply(state.skeleton);
                reward = GlobalVar.Lvl3Reward;
            }
        }
        private void Start()
        {
            TypeSwitch();
            StartCoroutine(MooveController());
        }

        void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("SnowballHippo"))
            {
                GM.EnemyHp(reward);
                StopAllCoroutines();
                StartCoroutine(Swap());

            }
        }

        IEnumerator Swap()
        {
            Debug.Log("Swap Start");
            Random rnd = new Random();
            
            yield return StartCoroutine(MooveToward(-12));
            yield return new WaitForSeconds(1);

            int change = rnd.Next(0, 3);
            Debug.Log("change = " + change);
            switch (change)
            {
                case 0:
                    type = EnemyType.lvl1;
                    break;
                case 1:
                    type = EnemyType.lvl2;
                    break;
                case 2:
                    type = EnemyType.lvl3;
                    break;
            }
            TypeSwitch();
            
            yield return StartCoroutine(MooveToward(-8));
            StartCoroutine(MooveController());
            yield return null;
        }
        
        IEnumerator MooveController()
        {
            var rnd = new Random();
            while (true)
            {
                int act = rnd.Next(1, 100);
                float point;
                if (act > chanceToStop)
                {
                    point = rnd.Next(-9, 9);
                    yield return StartCoroutine(MooveToward(point));
                }
                else
                {
                    int waitTime = rnd.Next(1, 3);
                    Debug.Log(gameObject.name + "|| Tut mi stoimim: "  + waitTime);
                    
                    yield return new WaitForSeconds(waitTime);
                    point = rnd.Next(-9, 9);
                    yield return StartCoroutine(MooveToward(point));
                }

                yield return null;
            }
            
            yield return null;
        }
        
        IEnumerator MooveToward(float point)
        {
            state.AnimationName = "run";
            if (_rigidbody.position.x < point)
                transform.rotation = Quaternion.Euler(0,0,0);
            else
                transform.rotation = Quaternion.Euler(0,180,0);
            
            
            while(transform.position.x != point)
            {
                var position = _rigidbody.position;
                _rigidbody.position =
                    Vector2.MoveTowards(position, new Vector2(point, position.y), Time.deltaTime * speed);
                yield return null;
            }
            state.AnimationName = "Idle";
            yield return null;
        }
    }
}
