﻿using System.Collections;
//using UnityEditorInternal;
using UnityEngine;

namespace Game
{
    public class HippoSB : MonoBehaviour
    {
        public HippoSBstate state;
        [SerializeField] 
        public float power;
        [SerializeField] 
        private Hippo _hippo;
        [SerializeField] 
        private SnowBallCD SnowCD;
    
        
        private Rigidbody2D m_rigidbody;
        private Vector3 oldposition;
        private float delta;
    
        
        private void Awake()
        {
            m_rigidbody = GetComponent<Rigidbody2D>();
            state = HippoSBstate.Wait;
            Debug.Log($"I`m Awake of {name}, Power: {power}");
        }
        
        void OnTriggerEnter2D(Collider2D col)
        {
            if (col.CompareTag("Enemy"))
            {
                StopAllCoroutines();
                StartCoroutine(EndForce());
            }
        }
        
        IEnumerator ForceSBcour()
        {
            state = HippoSBstate.Fly;
            m_rigidbody.position = _hippo.transform.position;
            m_rigidbody.AddRelativeForce( new Vector2( 0f, 5f ) * ((power) * 100));
            SnowCD.StartCour();
            Debug.Log("Force is enable");

            
            yield return new WaitForSeconds(0.9f);
            
            
            yield return StartCoroutine(EndForce());
            yield return null;
        }

        IEnumerator EndForce()
        {
            state = HippoSBstate.Wait;
            m_rigidbody.position = new Vector3(0, -17f, 0);
            yield return new WaitForFixedUpdate();
            gameObject.SetActive(false);
            yield return null;
        }
        
        public void ForceSB()
        {
            StartCoroutine(ForceSBcour());
        }
        
    }
}