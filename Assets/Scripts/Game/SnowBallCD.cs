﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class SnowBallCD : MonoBehaviour
{
    public bool snowballOnCd;
    
    [SerializeField] 
    public float snowballGlobalCD;
    [SerializeField]
    public float snowballCD;
    [SerializeField] 
    private Image CdView;
    [SerializeField] 
    private Image CdViewTemp;
    [SerializeField] 
    private Text textCd;

    private float Percent; 
    private float temp;
    
    private void Awake()
    {
        snowballCD = 0;
        snowballOnCd = false;
        temp = 0;
        Percent = snowballGlobalCD/1000 * 0.9f ;// ;
    }

    public void StartCour()
    {
        StartCoroutine(LoadSnowballCour());
    }
    
    private IEnumerator LoadSnowballCour()
    {
        snowballCD = snowballGlobalCD;
        textCd.enabled = true;
        snowballOnCd = true;
        temp = 0;
        CdView.fillAmount = 0;
        CdView.color = Color.yellow;
        CdViewTemp.fillAmount = 0;

        Percent = (snowballGlobalCD*0.9f)/10;
        
        while (snowballCD > 0)
        {
            snowballCD -= 1 * Time.deltaTime;
            temp += 1 * Time.deltaTime;
            textCd.text = string.Format("{0:0.0}",snowballCD);
            
            if (temp >= Percent)
            {
                CdView.fillAmount += 0.1f;
                CdViewTemp.fillAmount += 0.1f;
                temp = 0;
            }

            yield return typeof(WaitForEndOfFrame);
        }

        CdView.fillAmount = 1;
        CdView.color = Color.green;
        CdViewTemp.fillAmount = 1;
        snowballCD = 0;
        textCd.text = "" + 0;
        snowballOnCd = false;
        textCd.enabled = false;
        yield return null;
    }
}
