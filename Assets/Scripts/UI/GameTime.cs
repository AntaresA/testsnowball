﻿using UnityEngine;
using UnityEngine.UI;

public class GameTime : MonoBehaviour
{
    public Text Timer;

    private int _gameTime;

    private float _timeTemp;

    void Awake()
    {
        Timer.text = ""+0;
    }
    
    void Update()
    {
        Timer.text = ""+_gameTime;
        _timeTemp += 1 * Time.deltaTime;
        if (_timeTemp >= 1)
        {
            _gameTime += 1;
            _timeTemp = 0;
        }
    }
}
