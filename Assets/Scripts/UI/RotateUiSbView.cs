﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RotateUiSbView : MonoBehaviour
{
    [Header("Image to rotate")]
    [SerializeField]
    private Image Snowball;

    [Header("Rotation; (-) = Right, (+) = Left")]
    [SerializeField] 
    private float speed;
    

    // Update is called once per frame
    void FixedUpdate()
    {
        Snowball.transform.Rotate(0,0, speed);
    }
}
