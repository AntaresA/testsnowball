﻿
using UnityEngine;
using UnityEngine.UI;
public class WinWindow : MonoBehaviour
{
    [SerializeField]
    public int starQuantity;

    [SerializeField] public Image Star2;
    [SerializeField] public Image Star3;
    

    private void OnEnable()
    {
        if (starQuantity == 1)
        {
            Star3.color = Color.gray;
            Star2.color = Color.gray;
        }
        if (starQuantity == 2)
        {
            Star3.color = Color.gray;
        }
    }
}
